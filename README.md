# Task 10 - Copyright Strike

## About project

The application is a simplified iTunes. The app contains the following:

- Home page and search result page using thymeleaf as a templating engine. The user can search for a specific song in the Chinook database.
- REST endpoint containing data from SQL queries.

## Code structure 

The application is coded using the MVC pattern to achieve high cohesion and good coding pratices.

## Heroku

The application is published on Heroku. 

Link: https://copyright-strike.herokuapp.com/

