package no.noroff.itunes.controllers;

import no.noroff.itunes.data_access.CustomerRepository;
import no.noroff.itunes.models.Customer;
import no.noroff.itunes.models.PopularCustomerGenre;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Map;

@RestController
public class CustomerController {
    CustomerRepository customerRepository = new CustomerRepository();

    // Contains all customers:
    @RequestMapping(value = "/api/customers", method = RequestMethod.GET)
    public ArrayList<Customer> getAllCustomers(){
        return customerRepository.getAllCustomers();
    }

    @RequestMapping(value = "/api/customers/{id}", method = RequestMethod.GET)
    public Customer getSpecificCustomer(@PathVariable int id){
        return customerRepository.getSpecificCustomer(id);
    }

    @RequestMapping(value = "/api/customers", method = RequestMethod.POST)
    public Boolean addCustomer(@RequestBody Customer customer){
        return customerRepository.addCustomer(customer);
    }

    @RequestMapping(value = "/api/customers", method = RequestMethod.PUT)
    public Boolean updateCustomer(@RequestBody Customer customer){
        return customerRepository.updateCustomer(customer);
    }

    @RequestMapping(value = "/api/customers/spenders/highest", method = RequestMethod.GET)
    public Map<String, Integer> getHighestCustomerSpenders(){
        return customerRepository.getHighestCustomerSpenders();
    }

    @RequestMapping(value = "/api/customers/{id}/popular/genre", method = RequestMethod.GET)
    public ArrayList<PopularCustomerGenre> getCustomerPopularGenre(@PathVariable int id){
        return customerRepository.getCustomerPopularGenre(id);
    }

    @RequestMapping(value = "/api/countries/customers", method = RequestMethod.GET)
    public Map<String, Integer> getAllCountriesCustomers(){
        return customerRepository.getAllCountriesCustomers();
    }
}
