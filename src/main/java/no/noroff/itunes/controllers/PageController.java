package no.noroff.itunes.controllers;

import no.noroff.itunes.data_access.ArtistRepository;
import no.noroff.itunes.data_access.GenreRepository;
import no.noroff.itunes.data_access.TrackRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class PageController {

    ArtistRepository ar = new ArtistRepository();
    GenreRepository gr = new GenreRepository();
    TrackRepository tr = new TrackRepository();

    // Homepage with 5 random artists, genres and songs.
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home(Model model){
        model.addAttribute("artists", ar.getRandomArtists());
        model.addAttribute("genres", gr.getRandomGenres());
        model.addAttribute("tracks", tr.getRandomTracks());
        return "home";
    }

    // Search result page after user submits input.
    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public String getSpecificTrack(@RequestParam("term") String term, Model model){
        model.addAttribute("search", term); // For user info.
        model.addAttribute("track", tr.getSpecificTrack(term));
        return "searchResults";
    }
}
