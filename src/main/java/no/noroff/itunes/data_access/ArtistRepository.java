package no.noroff.itunes.data_access;

import no.noroff.itunes.models.Artist;
import no.noroff.itunes.models.Customer;

import java.sql.*;
import java.util.ArrayList;

public class ArtistRepository {
    // Connection setup.
    private final String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
    private Connection conn = null;


    // Returns 5 random artists for homepage.
    public ArrayList<Artist> getRandomArtists(){
        ArrayList<Artist> artists = new ArrayList<>();
        try{
            // connect
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("SELECT ArtistId, Name FROM Artist ORDER BY RANDOM() LIMIT 5");
            ResultSet set = prep.executeQuery();
            while(set.next()){
                artists.add( new Artist(
                        set.getInt("ArtistId"),
                        set.getString("Name")
                ));
            }
            System.out.println("Get random went well!");

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                conn.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }

        return artists;
    }
}
