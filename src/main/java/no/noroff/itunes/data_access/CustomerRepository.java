package no.noroff.itunes.data_access;

import no.noroff.itunes.models.Customer;
import no.noroff.itunes.models.PopularCustomerGenre;

import java.sql.*;
import java.util.*;

public class CustomerRepository {
    // Connection setup.
    private final String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
    private Connection conn = null;


    // Returns all customers.
    public ArrayList<Customer> getAllCustomers(){
        ArrayList<Customer> customers = new ArrayList<>();

        try{
            // connect
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement(
                            "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer");
            ResultSet set = prep.executeQuery();
            while(set.next()){
                customers.add( new Customer(
                        set.getInt("CustomerId"),
                        set.getString("FirstName"),
                        set.getString("LastName"),
                        set.getString("Country"),
                        set.getString("PostalCode"),
                        set.getString("Phone"),
                        set.getString("Email")
                ));
            }
            System.out.println("Get all customers went well!");

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                conn.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }

        return customers;
    }


    // Returns a Customer() by id.
    public Customer getSpecificCustomer(int id){
        Customer customer = null;

        try{
            // connect
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement(
                            "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email " +
                            "FROM Customer WHERE CustomerId=?");
            prep.setInt(1,id);
            ResultSet set = prep.executeQuery();
            while(set.next()){
                customer = new Customer(
                        set.getInt("CustomerId"),
                        set.getString("FirstName"),
                        set.getString("LastName"),
                        set.getString("Country"),
                        set.getString("PostalCode"),
                        set.getString("Phone"),
                        set.getString("Email")
                );
            }
            System.out.println("Get specific customer went well!");

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                conn.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }

        return customer;
    }


    // Returns the number of customers in each country ordered descending.
    public Map<String, Integer> getAllCountriesCustomers(){
        Map<String, Integer> countryCustomers = new LinkedHashMap<>();

        try{
            // connect
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement(
                            "SELECT Country, COUNT(*) as Customers FROM Customer GROUP BY Country ORDER BY customers DESC");
            ResultSet set = prep.executeQuery();
            while(set.next()){
                countryCustomers.put(set.getString("Country"), set.getInt("Customers"));
            }
            System.out.println("Get customers in countries went well!");

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                conn.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }

        return countryCustomers;
    }


    // Returns a list of customers and the amount spent ordered descending.
    public Map<String, Integer> getHighestCustomerSpenders() {
        Map<String, Integer> highestSpender = new LinkedHashMap<>();

        try {
            // connect
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement(
                            "SELECT FirstName || ' ' || LastName AS CustomerName, sum(Total) AS Total FROM Customer "
                            + "INNER JOIN Invoice ON Customer.CustomerId=Invoice.CustomerId GROUP BY CustomerName ORDER BY Total DESC");
            ResultSet set = prep.executeQuery();
            while (set.next()) {
                highestSpender.put(set.getString("CustomerName"), set.getInt("Total"));
            }
            System.out.println("Get highest spender customers went well!");

        } catch (Exception exception) {
            System.out.println(exception.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception exception) {
                System.out.println(exception.toString());
            }
        }

        return highestSpender;
    }


    // Returns the most popular genre for a specific customer.
    // In case of a tie, display both.
    public ArrayList<PopularCustomerGenre> getCustomerPopularGenre(int id){
        ArrayList<PopularCustomerGenre> popularGenre = new ArrayList<>();

        try{
            // connect
            conn = DriverManager.getConnection(URL);
            // t1: list of most invoices for each genres for a specific customer ordered descending.
            // t2: finds the most popular of the genres for the person in t1.
            PreparedStatement prep =
                    conn.prepareStatement(
                            "WITH t1 AS (" +
                                    "SELECT FirstName || ' ' || LastName AS CustomerName, g.Name as gName, COUNT(i.InvoiceId) AS Purchases FROM Invoice i " +
                                    "JOIN Customer c ON i.CustomerId = c.CustomerId " +
                                    "JOIN InvoiceLine il ON il.Invoiceid = i.InvoiceId " +
                                    "JOIN Track t ON t.TrackId = il.Trackid " +
                                    "JOIN Genre g ON t.GenreId = g.GenreId " +
                                    "WHERE c.CustomerId=?" +
                                    "GROUP BY CustomerName, g.Name " +
                                    "ORDER BY CustomerName, Purchases DESC) " +
                                    "SELECT t1.* FROM t1 " +
                                    "JOIN (SELECT CustomerName, gName, MAX(Purchases) AS MaxPurchases " +
                                    "FROM t1 GROUP BY CustomerName)t2 " +
                                    "ON t1.CustomerName = t2.CustomerName " +
                                    "WHERE t1.Purchases = t2.MaxPurchases;");
            prep.setInt(1,id);
            ResultSet set = prep.executeQuery();
            while(set.next()){
                popularGenre.add( new PopularCustomerGenre(
                        set.getString("CustomerName"),
                        set.getString("gName"),
                        set.getInt("Purchases")
                ));
            }
            System.out.println("Get most popular genre for given customer went well!");

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                conn.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }

        return popularGenre;
    }


    // Returns true if the added customer succeeded, else false.
    public Boolean addCustomer(Customer customer){
        Boolean success = false;
        try{
            // connect
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement(
                            "INSERT INTO Customer(CustomerId,FirstName,LastName, Country, PostalCode, Phone, Email)" +
                            " VALUES(?,?,?,?,?,?,?)");
            prep.setInt(1,customer.getCustomerId());
            prep.setString(2,customer.getFirstName());
            prep.setString(3,customer.getLastName());
            prep.setString(4,customer.getCountry());
            prep.setString(5,customer.getPostalCode());
            prep.setString(6,customer.getPhone());
            prep.setString(7,customer.getEmail());

            int result = prep.executeUpdate();
            success = (result != 0); // if res = 1; true

            System.out.println("Add customer went well!");

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                conn.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }

        return success;
    }


    // Returns true if the updated customer succeeded, else false.
    public Boolean updateCustomer(Customer customer){
        Boolean success = false;
        try{
            // connect
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement(
                            "UPDATE Customer SET CustomerId=?, FirstName=?, LastName=?, Country=?, PostalCode=?, Phone=?, Email=?" +
                            " WHERE CustomerId=?");
            prep.setInt(1,customer.getCustomerId());
            prep.setString(2,customer.getFirstName());
            prep.setString(3,customer.getLastName());
            prep.setString(4,customer.getCountry());
            prep.setString(5,customer.getPostalCode());
            prep.setString(6,customer.getPhone());
            prep.setString(7,customer.getEmail());
            prep.setInt(8,customer.getCustomerId());

            int result = prep.executeUpdate();
            success = (result != 0); // if res = 1; true

            System.out.println("Update customer went well!");

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                conn.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }

        return success;
    }
}
