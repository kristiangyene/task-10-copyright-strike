package no.noroff.itunes.data_access;

import no.noroff.itunes.models.Genre;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class GenreRepository {
    // Connection setup.
    private final String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
    private Connection conn = null;


    // Returns 5 random genres for homepage.
    public ArrayList<Genre> getRandomGenres(){
        ArrayList<Genre> genres = new ArrayList<>();

        try{
            // connect
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("SELECT GenreId, Name FROM Genre  ORDER BY RANDOM() LIMIT 5");
            ResultSet set = prep.executeQuery();
            while(set.next()){
                genres.add( new Genre(
                        set.getInt("GenreId"),
                        set.getString("Name")
                ));
            }
            System.out.println("Get random genres went well!");

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                conn.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }

        return genres;
    }
}
