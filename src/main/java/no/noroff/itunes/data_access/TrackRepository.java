package no.noroff.itunes.data_access;

import no.noroff.itunes.models.Customer;
import no.noroff.itunes.models.Track;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class TrackRepository {
    // Connection setup.
    private final String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
    private Connection conn = null;


    // Returns 5 random tracks for homepage.
    public ArrayList<Track> getRandomTracks(){
        ArrayList<Track> tracks = new ArrayList<>();

        try{
            // connect
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("SELECT TrackId, Name FROM Track  ORDER BY RANDOM() LIMIT 5");
            ResultSet set = prep.executeQuery();
            while(set.next()){
                tracks.add( new Track(
                        set.getInt("TrackId"),
                        set.getString("Name")
                ));
            }
            System.out.println("Get random tracks went well!");

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                conn.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }

        return tracks;
    }

    // Get a specific song by name requested by the user.
    public Track getSpecificTrack(String name){
        Track track = null;

        try{
            // connect
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement(
                            "SELECT t.TrackId as tId, t.Name as tName, ar.Name as arName, al.Title as alTitle, g.Name as gName " +
                            "FROM Track t " +
                            "INNER JOIN Album al ON t.AlbumId=al.AlbumId " +
                            "INNER JOIN Artist ar ON al.ArtistId=ar.ArtistId " +
                            "INNER JOIN Genre g ON t.GenreId=g.GenreId " +
                            "WHERE LOWER(t.Name)=?");
            prep.setString(1, name.toLowerCase());
            ResultSet set = prep.executeQuery();
            while(set.next()){
                track = new Track(
                        set.getInt("tId"),
                        set.getString("tName"),
                        set.getString("arName"),
                        set.getString("alTitle"),
                        set.getString("gName")
                );
            }
            System.out.println("Get specific track went well!");

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                conn.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }

        return track;

    }
}
