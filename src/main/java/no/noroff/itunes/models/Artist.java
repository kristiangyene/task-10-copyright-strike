package no.noroff.itunes.models;

public class Artist {
    private int artistId;
    private String name;

    // Constructors.
    public Artist(){
    }

    public Artist(int artistId, String name) {
        this.artistId = artistId;
        this.name = name;
    }

    // Getters and setters.
    public int getArtistId() {
        return artistId;
    }

    public void setArtistId(int artistId) {
        this.artistId = artistId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
