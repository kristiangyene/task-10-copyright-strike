package no.noroff.itunes.models;

public class Country {
    private String name;
    private int customers;

    // Constructors.
    public Country() {
    }

    public Country(String name, int customers) {
        this.name = name;
        this.customers = customers;
    }

    // Getters and setters.
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCustomers() {
        return customers;
    }

    public void setCustomers(int customers) {
        this.customers = customers;
    }
}
