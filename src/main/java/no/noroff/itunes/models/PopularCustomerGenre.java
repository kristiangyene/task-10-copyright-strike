package no.noroff.itunes.models;

public class PopularCustomerGenre {
    private String customerName;
    private String genreName;
    private int purchases;

    // Constructors.
    public PopularCustomerGenre() {
    }

    public PopularCustomerGenre(String customerName, String genreName, int purchases) {
        this.customerName = customerName;
        this.genreName = genreName;
        this.purchases = purchases;
    }

    // Getters and setters.
    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getGenreName() {
        return genreName;
    }

    public void setGenreName(String genreName) {
        this.genreName = genreName;
    }

    public int getPurchases() {
        return purchases;
    }

    public void setPurchases(int purchases) {
        this.purchases = purchases;
    }
}
