package no.noroff.itunes.models;

public class Track {
    private int trackId;
    private String name;
    private String artistName;
    private String albumName;
    private String genreName;

    // Constructors.
    public Track() {
    }

    public Track(int trackId, String name) {
        this.trackId = trackId;
        this.name = name;
    }

    public Track(int trackId, String name, String artistName, String albumName, String genreName) {
        this.trackId = trackId;
        this.name = name;
        this.artistName = artistName;
        this.albumName = albumName;
        this.genreName = genreName;
    }

    // Getters and setters.
    public int getTrackId() {
        return trackId;
    }

    public void setTrackId(int trackId) {
        this.trackId = trackId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArtistName() {
        return artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    public String getAlbumName() {
        return albumName;
    }

    public void setAlbumName(String albumName) {
        this.albumName = albumName;
    }

    public String getGenreName() {
        return genreName;
    }

    public void setGenreName(String genreName) {
        this.genreName = genreName;
    }
}